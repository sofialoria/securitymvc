package com.accenture.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.model.User;
import com.accenture.repository.PeliculaDao;
import com.accenture.repository.UserRepositoryDao;

@RestController
public class MainController {
	
	
	@Autowired
	UserRepositoryDao userDao;
	@Autowired
	PeliculaDao peliculadao;
	
	
	
	@PostMapping({"/admin/add"})
	public User crearUsuario(@RequestBody User user){
		
		
		return userDao.save(user);
		
	}

//	@GetMapping({"/pleiculas"})
//	public ResponseEntity<Object> findAll() {
//		
//		List<Pelicula> peliculasFound = peliculadao.findAll();
//		
//		if(peliculasFound.size() > 0) {
//			
//			for(Pelicula peli: peliculasFound) {
//				
//				System.out.println("Nombre: " +peli.getTitulo());
//			}
//		} else {
//			
//			JSONObject obj = new JSONObject();
//			
//			obj.put("error", 1);
//			obj.put("results", "no se encontraron pleiculas");
//			
//			
//			return ResponseEntity.ok().body(obj.toString());
//			
//		}
//		
//	}
	
}
