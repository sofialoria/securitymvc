package com.accenture.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.model.User;

public interface UserRepositoryDao extends JpaRepository<User, Long> { 
	
	User findByUsername(String username);

}
