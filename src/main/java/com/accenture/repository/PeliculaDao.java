package com.accenture.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.model.Pelicula;

public interface PeliculaDao extends JpaRepository<Pelicula, Long> {

}
