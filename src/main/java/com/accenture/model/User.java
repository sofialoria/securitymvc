package com.accenture.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
//Propirties
//creacion de la tabla

	@Id
	private Long id;
	private String username;
	private String pass;
	private String role;
	
	
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
//	private List<Pelicula> peliculas;
//	
//	
//	
//	public List<Pelicula> getPeliculas(){
//		return peliculas;
//	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getRole() {
		return role;
	}
	public void setRole() {
		this.role = role;
	}
}