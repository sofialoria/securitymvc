package com.accenture.demoSecurityApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.accenture.model.User;
import com.accenture.repository.UserRepositoryDao;

@Service
public class MyUserDitails implements UserDetailsService {

	

	@Autowired
	private UserRepositoryDao repoUser;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User usuario = repoUser.findByUsername(username);
		if(usuario == null) {
			throw new UsernameNotFoundException("El usuario no existe");
		}
		
		return new UserPrincipal(usuario);
	}
}




//	
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//		User user = repoUser.findByUsername(username);
//		if(user == null) {
//			throw new UsernameNotFoundException("No se encontro el usuario");
//			
//		}
//		return new UserPrincipal(user);
//	}